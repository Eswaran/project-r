# Project R

### Main Dependencies

1.  React ^16.x
2.  React-Redux
3.  Redux-Saga 0.16.x
4.  Material UI – 4
5.  React-router 4.x
6.  Redux-Action
7.  Redux-Persist 5.x
8.  React-Loadable
9.  React-Helmet 5.x
10. styled-components 4.x
11. React-JSS
12. Redux-logger
13. React-DND

### Unit Testing

1.  jest 23.x
2.  enzyme 3.x

`yarn test`

### Development

1.  webpack-dev-server 3.x
2.  react-hot-loader 4.x
3.  redux-devtools (with browser plugin)

`yarn start`

### Building

1.  webpack 4.x
2.  babel 7.x

`yarn build`

### Code Quality

1.  eslint 5.x
2.  stylelint 9.x

`yarn lint` / `yarn lint:styles`

### Architecture Diagram

![Architecture Diagram](./assets_media_images_achitectureDiag.png)

### Folder Structure

```
.
├── ...
├── views
|	├── RouterContainer.js
|	├── themeConfig.js
|	├── rootReducer.js
|	├── rootSaga.js
|	├── login_page
|	|	├── container
|	|	|	└── LoginContainer.js
|	|	├── component
|	|	|	└── FormComponent.js
|	|	├── reducer
|	|	|	└── loginReducer.js
|	|	├── actions
|	|	|	└── loginActions.js
|	|	├── sagas
|	|	|	└── loginSaga.js
|	|	└── test
|	|		├──LoginContainer.spec.js
|	|		├──FormComponent.spec.js
|	|		├──loginReducer.spec.js
|	|		├──loginActions.spec.js
|	|		└── loginSaga.spec.js
|	├── common_component
|	|	└──FormValidation.js
|	└── tools/Utils
|		├── currencyConverter.js
|		└── timeConverter.js
├──constants
|	└── index.js
├── modules
├── store
└── ...
```

## Project R (React Style Guide)

## Basic Rules

- Only include one React component per file.
  - However, multiple [Stateless, or Pure, Components](https://facebook.github.io/react/docs/reusable-components.html#stateless-functions) are allowed per file. eslint: [`react/no-multi-comp`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-multi-comp.md#ignorestateless).
- [`react/propTypes`](<[https://www.npmjs.com/package/prop-types](https://www.npmjs.com/package/prop-types)>) will allow to catch a lot of bugs with typechecking.
- Use try & catch blocks only for Sagas, use [`errorBoundries`](<[[https://reactjs.org/docs/error-boundaries.html](https://reactjs.org/docs/error-boundaries.html)]>) for **catch JavaScript errors**.

#### Naming Conventions

- **Extensions**: Use `.js` extension for React components.

- **Filename**: Component and Container File Name Use PascalCase. E.g., `AccountsPageContainer and not accountsPageContainer`.
- **Events**: Event functions should have prefix **handle\*** keyword `eg: handleOnCLickLogin()`
- **Reference Naming**: Use PascalCase for React components and camelCase for their instances. eslint: [`react/jsx-pascal-case`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-pascal-case.md)

  `const reservationItem = <ReservationCard />;`

## Props

- Always use camelCase for prop names.
  `<Foo userName="hello" phoneNumber={12345678} />`
- Omit the value of the prop when it is explicitly `true`.

  `<Foo hidden={true} /> to this <Foo hidden />`

- Always include an `alt` prop on `<img>` tags. If the image is presentational, `alt` can be an empty string or the `<img>` must have `role="presentation"`.

  ```
      <img src="hello.jpg" alt="Me waving hello" /> or
      <img src="hello.jpg" alt="" /> or
      <img src="hello.jpg" role="presentation" />
  ```

- Avoid using an array index as `key` prop, prefer a stable ID.
  We don’t recommend using indexes for keys if the order of items may change
  `{todos.map(todo => ( <Todo {...todo} key={todo.id} /> ))}`

## Refs

Always use ref callbacks. eslint: [`react/no-string-refs`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-string-refs.md)

```
    <Foo
      ref="myRef"
    />
    To below
    <Foo
      ref={(ref) => { this.myRef = ref; }}
    />
```

## Tags

Always self-close tags that have no children.

```
   <Foo variant="stuff"></Foo>
   To below
   <Foo variant="stuff" />
```

## Methods

- Use arrow functions to close over local variables. It is handy when you need to pass additional data to an event handler.
  ```
      function ItemList(props) {
        return (
          <ul>
            {props.items.map((item, index) => (
              <Item
                key={item.key}
                onClick={(event) => doSomethingWith(event, item.name, index)}
              />
            ))}
          </ul>
        );
      }
  ```
- Bind event handlers for the render method in the constructor. eslint: [`react/jsx-no-bind`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md)

  ```
  	class extends React.Component {
        constructor(props) {
          super(props);

          this.onClickDiv = this.onClickDiv.bind(this);
        }

        onClickDiv() {
          // do stuff
        }

        render() {
          return <div onClick={this.onClickDiv} />;
        }
      }
  ```

- Be sure to return a value in your `render` methods. eslint: [`react/require-render-return`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/require-render-return.md)

  ```
      render() {
        return (<div />);
      }
  ```
